/**
 */
package no.tru.coursework;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.tru.coursework.Person#getName <em>Name</em>}</li>
 *   <li>{@link no.tru.coursework.Person#getPhoneNo <em>Phone No</em>}</li>
 * </ul>
 *
 * @see no.tru.coursework.CourseworkPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see no.tru.coursework.CourseworkPackage#getPerson_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Person#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Phone No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Phone No</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phone No</em>' attribute.
	 * @see #setPhoneNo(String)
	 * @see no.tru.coursework.CourseworkPackage#getPerson_PhoneNo()
	 * @model
	 * @generated
	 */
	String getPhoneNo();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Person#getPhoneNo <em>Phone No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phone No</em>' attribute.
	 * @see #getPhoneNo()
	 * @generated
	 */
	void setPhoneNo(String value);

} // Person
