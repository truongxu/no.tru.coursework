/**
 */
package no.tru.coursework;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.tru.coursework.Course#getCourseName <em>Course Name</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getCourseDescription <em>Course Description</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getCourseCredit <em>Course Credit</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getPersonCoordinator <em>Person Coordinator</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getPersonLecture <em>Person Lecture</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link no.tru.coursework.Course#getTimetable <em>Timetable</em>}</li>
 * </ul>
 *
 * @see no.tru.coursework.CourseworkPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Course Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Name</em>' attribute.
	 * @see #setCourseName(String)
	 * @see no.tru.coursework.CourseworkPackage#getCourse_CourseName()
	 * @model
	 * @generated
	 */
	String getCourseName();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Course#getCourseName <em>Course Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Name</em>' attribute.
	 * @see #getCourseName()
	 * @generated
	 */
	void setCourseName(String value);

	/**
	 * Returns the value of the '<em><b>Course Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Code</em>' attribute.
	 * @see #setCourseCode(String)
	 * @see no.tru.coursework.CourseworkPackage#getCourse_CourseCode()
	 * @model
	 * @generated
	 */
	String getCourseCode();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Course#getCourseCode <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Code</em>' attribute.
	 * @see #getCourseCode()
	 * @generated
	 */
	void setCourseCode(String value);

	/**
	 * Returns the value of the '<em><b>Course Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Description</em>' attribute.
	 * @see #setCourseDescription(String)
	 * @see no.tru.coursework.CourseworkPackage#getCourse_CourseDescription()
	 * @model
	 * @generated
	 */
	String getCourseDescription();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Course#getCourseDescription <em>Course Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Description</em>' attribute.
	 * @see #getCourseDescription()
	 * @generated
	 */
	void setCourseDescription(String value);

	/**
	 * Returns the value of the '<em><b>Course Credit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Credit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Credit</em>' attribute.
	 * @see #setCourseCredit(double)
	 * @see no.tru.coursework.CourseworkPackage#getCourse_CourseCredit()
	 * @model
	 * @generated
	 */
	double getCourseCredit();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Course#getCourseCredit <em>Course Credit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Credit</em>' attribute.
	 * @see #getCourseCredit()
	 * @generated
	 */
	void setCourseCredit(double value);

	/**
	 * Returns the value of the '<em><b>Person Coordinator</b></em>' containment reference list.
	 * The list contents are of type {@link no.tru.coursework.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Coordinator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Coordinator</em>' containment reference list.
	 * @see no.tru.coursework.CourseworkPackage#getCourse_PersonCoordinator()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getPersonCoordinator();

	/**
	 * Returns the value of the '<em><b>Person Lecture</b></em>' containment reference list.
	 * The list contents are of type {@link no.tru.coursework.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Person Lecture</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person Lecture</em>' containment reference list.
	 * @see no.tru.coursework.CourseworkPackage#getCourse_PersonLecture()
	 * @model containment="true"
	 * @generated
	 */
	EList<Person> getPersonLecture();

	/**
	 * Returns the value of the '<em><b>Evaluationform</b></em>' containment reference list.
	 * The list contents are of type {@link no.tru.coursework.EvaluationForm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationform</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationform</em>' containment reference list.
	 * @see no.tru.coursework.CourseworkPackage#getCourse_Evaluationform()
	 * @model containment="true"
	 * @generated
	 */
	EList<EvaluationForm> getEvaluationform();

	/**
	 * Returns the value of the '<em><b>Coursework</b></em>' containment reference list.
	 * The list contents are of type {@link no.tru.coursework.CourseWork}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coursework</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coursework</em>' containment reference list.
	 * @see no.tru.coursework.CourseworkPackage#getCourse_Coursework()
	 * @model containment="true"
	 * @generated
	 */
	EList<CourseWork> getCoursework();

	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' reference.
	 * @see #setTimetable(Timetable)
	 * @see no.tru.coursework.CourseworkPackage#getCourse_Timetable()
	 * @model
	 * @generated
	 */
	Timetable getTimetable();

	/**
	 * Sets the value of the '{@link no.tru.coursework.Course#getTimetable <em>Timetable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(Timetable value);

} // Course
