/**
 */
package no.tru.coursework;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Work</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.tru.coursework.CourseWork#getLectureHour <em>Lecture Hour</em>}</li>
 *   <li>{@link no.tru.coursework.CourseWork#getLabhours <em>Labhours</em>}</li>
 *   <li>{@link no.tru.coursework.CourseWork#getCourseTerm <em>Course Term</em>}</li>
 * </ul>
 *
 * @see no.tru.coursework.CourseworkPackage#getCourseWork()
 * @model
 * @generated
 */
public interface CourseWork extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecture Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hour</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hour</em>' attribute.
	 * @see #setLectureHour(double)
	 * @see no.tru.coursework.CourseworkPackage#getCourseWork_LectureHour()
	 * @model
	 * @generated
	 */
	double getLectureHour();

	/**
	 * Sets the value of the '{@link no.tru.coursework.CourseWork#getLectureHour <em>Lecture Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Hour</em>' attribute.
	 * @see #getLectureHour()
	 * @generated
	 */
	void setLectureHour(double value);

	/**
	 * Returns the value of the '<em><b>Labhours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Labhours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Labhours</em>' attribute.
	 * @see #setLabhours(double)
	 * @see no.tru.coursework.CourseworkPackage#getCourseWork_Labhours()
	 * @model
	 * @generated
	 */
	double getLabhours();

	/**
	 * Sets the value of the '{@link no.tru.coursework.CourseWork#getLabhours <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Labhours</em>' attribute.
	 * @see #getLabhours()
	 * @generated
	 */
	void setLabhours(double value);

	/**
	 * Returns the value of the '<em><b>Course Term</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Term</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Term</em>' attribute.
	 * @see #setCourseTerm(String)
	 * @see no.tru.coursework.CourseworkPackage#getCourseWork_CourseTerm()
	 * @model
	 * @generated
	 */
	String getCourseTerm();

	/**
	 * Sets the value of the '{@link no.tru.coursework.CourseWork#getCourseTerm <em>Course Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Term</em>' attribute.
	 * @see #getCourseTerm()
	 * @generated
	 */
	void setCourseTerm(String value);

} // CourseWork
