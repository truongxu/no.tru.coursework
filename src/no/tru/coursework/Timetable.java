/**
 */
package no.tru.coursework;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see no.tru.coursework.CourseworkPackage#getTimetable()
 * @model
 * @generated
 */
public interface Timetable extends EObject {
} // Timetable
