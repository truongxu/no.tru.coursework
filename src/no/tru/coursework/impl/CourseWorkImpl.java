/**
 */
package no.tru.coursework.impl;

import no.tru.coursework.CourseWork;
import no.tru.coursework.CourseworkPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Work</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.tru.coursework.impl.CourseWorkImpl#getLectureHour <em>Lecture Hour</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseWorkImpl#getLabhours <em>Labhours</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseWorkImpl#getCourseTerm <em>Course Term</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseWorkImpl extends MinimalEObjectImpl.Container implements CourseWork {
	/**
	 * The default value of the '{@link #getLectureHour() <em>Lecture Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHour()
	 * @generated
	 * @ordered
	 */
	protected static final double LECTURE_HOUR_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLectureHour() <em>Lecture Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHour()
	 * @generated
	 * @ordered
	 */
	protected double lectureHour = LECTURE_HOUR_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabhours() <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabhours()
	 * @generated
	 * @ordered
	 */
	protected static final double LABHOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLabhours() <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabhours()
	 * @generated
	 * @ordered
	 */
	protected double labhours = LABHOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseTerm() <em>Course Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseTerm()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_TERM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseTerm() <em>Course Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseTerm()
	 * @generated
	 * @ordered
	 */
	protected String courseTerm = COURSE_TERM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseWorkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseworkPackage.Literals.COURSE_WORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLectureHour() {
		return lectureHour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLectureHour(double newLectureHour) {
		double oldLectureHour = lectureHour;
		lectureHour = newLectureHour;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE_WORK__LECTURE_HOUR,
					oldLectureHour, lectureHour));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLabhours() {
		return labhours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabhours(double newLabhours) {
		double oldLabhours = labhours;
		labhours = newLabhours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE_WORK__LABHOURS, oldLabhours,
					labhours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseTerm() {
		return courseTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseTerm(String newCourseTerm) {
		String oldCourseTerm = courseTerm;
		courseTerm = newCourseTerm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE_WORK__COURSE_TERM,
					oldCourseTerm, courseTerm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CourseworkPackage.COURSE_WORK__LECTURE_HOUR:
			return getLectureHour();
		case CourseworkPackage.COURSE_WORK__LABHOURS:
			return getLabhours();
		case CourseworkPackage.COURSE_WORK__COURSE_TERM:
			return getCourseTerm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CourseworkPackage.COURSE_WORK__LECTURE_HOUR:
			setLectureHour((Double) newValue);
			return;
		case CourseworkPackage.COURSE_WORK__LABHOURS:
			setLabhours((Double) newValue);
			return;
		case CourseworkPackage.COURSE_WORK__COURSE_TERM:
			setCourseTerm((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CourseworkPackage.COURSE_WORK__LECTURE_HOUR:
			setLectureHour(LECTURE_HOUR_EDEFAULT);
			return;
		case CourseworkPackage.COURSE_WORK__LABHOURS:
			setLabhours(LABHOURS_EDEFAULT);
			return;
		case CourseworkPackage.COURSE_WORK__COURSE_TERM:
			setCourseTerm(COURSE_TERM_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CourseworkPackage.COURSE_WORK__LECTURE_HOUR:
			return lectureHour != LECTURE_HOUR_EDEFAULT;
		case CourseworkPackage.COURSE_WORK__LABHOURS:
			return labhours != LABHOURS_EDEFAULT;
		case CourseworkPackage.COURSE_WORK__COURSE_TERM:
			return COURSE_TERM_EDEFAULT == null ? courseTerm != null : !COURSE_TERM_EDEFAULT.equals(courseTerm);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lectureHour: ");
		result.append(lectureHour);
		result.append(", labhours: ");
		result.append(labhours);
		result.append(", courseTerm: ");
		result.append(courseTerm);
		result.append(')');
		return result.toString();
	}

} //CourseWorkImpl
