/**
 */
package no.tru.coursework.impl;

import java.util.Collection;

import no.tru.coursework.Course;
import no.tru.coursework.CourseWork;
import no.tru.coursework.CourseworkPackage;
import no.tru.coursework.EvaluationForm;
import no.tru.coursework.Person;
import no.tru.coursework.Timetable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getCourseName <em>Course Name</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getCourseDescription <em>Course Description</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getCourseCredit <em>Course Credit</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getPersonCoordinator <em>Person Coordinator</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getPersonLecture <em>Person Lecture</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link no.tru.coursework.impl.CourseImpl#getTimetable <em>Timetable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCourseName() <em>Course Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseName()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseName() <em>Course Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseName()
	 * @generated
	 * @ordered
	 */
	protected String courseName = COURSE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected String courseCode = COURSE_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseDescription() <em>Course Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseDescription() <em>Course Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseDescription()
	 * @generated
	 * @ordered
	 */
	protected String courseDescription = COURSE_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCourseCredit() <em>Course Credit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCredit()
	 * @generated
	 * @ordered
	 */
	protected static final double COURSE_CREDIT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCourseCredit() <em>Course Credit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCredit()
	 * @generated
	 * @ordered
	 */
	protected double courseCredit = COURSE_CREDIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPersonCoordinator() <em>Person Coordinator</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonCoordinator()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> personCoordinator;

	/**
	 * The cached value of the '{@link #getPersonLecture() <em>Person Lecture</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersonLecture()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> personLecture;

	/**
	 * The cached value of the '{@link #getEvaluationform() <em>Evaluationform</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationform()
	 * @generated
	 * @ordered
	 */
	protected EList<EvaluationForm> evaluationform;

	/**
	 * The cached value of the '{@link #getCoursework() <em>Coursework</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoursework()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseWork> coursework;

	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected Timetable timetable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseworkPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseName(String newCourseName) {
		String oldCourseName = courseName;
		courseName = newCourseName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE__COURSE_NAME, oldCourseName,
					courseName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseCode() {
		return courseCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseCode(String newCourseCode) {
		String oldCourseCode = courseCode;
		courseCode = newCourseCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE__COURSE_CODE, oldCourseCode,
					courseCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseDescription() {
		return courseDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseDescription(String newCourseDescription) {
		String oldCourseDescription = courseDescription;
		courseDescription = newCourseDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE__COURSE_DESCRIPTION,
					oldCourseDescription, courseDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCourseCredit() {
		return courseCredit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseCredit(double newCourseCredit) {
		double oldCourseCredit = courseCredit;
		courseCredit = newCourseCredit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE__COURSE_CREDIT,
					oldCourseCredit, courseCredit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersonCoordinator() {
		if (personCoordinator == null) {
			personCoordinator = new EObjectContainmentEList<Person>(Person.class, this,
					CourseworkPackage.COURSE__PERSON_COORDINATOR);
		}
		return personCoordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Person> getPersonLecture() {
		if (personLecture == null) {
			personLecture = new EObjectContainmentEList<Person>(Person.class, this,
					CourseworkPackage.COURSE__PERSON_LECTURE);
		}
		return personLecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EvaluationForm> getEvaluationform() {
		if (evaluationform == null) {
			evaluationform = new EObjectContainmentEList<EvaluationForm>(EvaluationForm.class, this,
					CourseworkPackage.COURSE__EVALUATIONFORM);
		}
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseWork> getCoursework() {
		if (coursework == null) {
			coursework = new EObjectContainmentEList<CourseWork>(CourseWork.class, this,
					CourseworkPackage.COURSE__COURSEWORK);
		}
		return coursework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable getTimetable() {
		if (timetable != null && timetable.eIsProxy()) {
			InternalEObject oldTimetable = (InternalEObject) timetable;
			timetable = (Timetable) eResolveProxy(oldTimetable);
			if (timetable != oldTimetable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CourseworkPackage.COURSE__TIMETABLE,
							oldTimetable, timetable));
			}
		}
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable basicGetTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(Timetable newTimetable) {
		Timetable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseworkPackage.COURSE__TIMETABLE, oldTimetable,
					timetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CourseworkPackage.COURSE__PERSON_COORDINATOR:
			return ((InternalEList<?>) getPersonCoordinator()).basicRemove(otherEnd, msgs);
		case CourseworkPackage.COURSE__PERSON_LECTURE:
			return ((InternalEList<?>) getPersonLecture()).basicRemove(otherEnd, msgs);
		case CourseworkPackage.COURSE__EVALUATIONFORM:
			return ((InternalEList<?>) getEvaluationform()).basicRemove(otherEnd, msgs);
		case CourseworkPackage.COURSE__COURSEWORK:
			return ((InternalEList<?>) getCoursework()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CourseworkPackage.COURSE__COURSE_NAME:
			return getCourseName();
		case CourseworkPackage.COURSE__COURSE_CODE:
			return getCourseCode();
		case CourseworkPackage.COURSE__COURSE_DESCRIPTION:
			return getCourseDescription();
		case CourseworkPackage.COURSE__COURSE_CREDIT:
			return getCourseCredit();
		case CourseworkPackage.COURSE__PERSON_COORDINATOR:
			return getPersonCoordinator();
		case CourseworkPackage.COURSE__PERSON_LECTURE:
			return getPersonLecture();
		case CourseworkPackage.COURSE__EVALUATIONFORM:
			return getEvaluationform();
		case CourseworkPackage.COURSE__COURSEWORK:
			return getCoursework();
		case CourseworkPackage.COURSE__TIMETABLE:
			if (resolve)
				return getTimetable();
			return basicGetTimetable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CourseworkPackage.COURSE__COURSE_NAME:
			setCourseName((String) newValue);
			return;
		case CourseworkPackage.COURSE__COURSE_CODE:
			setCourseCode((String) newValue);
			return;
		case CourseworkPackage.COURSE__COURSE_DESCRIPTION:
			setCourseDescription((String) newValue);
			return;
		case CourseworkPackage.COURSE__COURSE_CREDIT:
			setCourseCredit((Double) newValue);
			return;
		case CourseworkPackage.COURSE__PERSON_COORDINATOR:
			getPersonCoordinator().clear();
			getPersonCoordinator().addAll((Collection<? extends Person>) newValue);
			return;
		case CourseworkPackage.COURSE__PERSON_LECTURE:
			getPersonLecture().clear();
			getPersonLecture().addAll((Collection<? extends Person>) newValue);
			return;
		case CourseworkPackage.COURSE__EVALUATIONFORM:
			getEvaluationform().clear();
			getEvaluationform().addAll((Collection<? extends EvaluationForm>) newValue);
			return;
		case CourseworkPackage.COURSE__COURSEWORK:
			getCoursework().clear();
			getCoursework().addAll((Collection<? extends CourseWork>) newValue);
			return;
		case CourseworkPackage.COURSE__TIMETABLE:
			setTimetable((Timetable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CourseworkPackage.COURSE__COURSE_NAME:
			setCourseName(COURSE_NAME_EDEFAULT);
			return;
		case CourseworkPackage.COURSE__COURSE_CODE:
			setCourseCode(COURSE_CODE_EDEFAULT);
			return;
		case CourseworkPackage.COURSE__COURSE_DESCRIPTION:
			setCourseDescription(COURSE_DESCRIPTION_EDEFAULT);
			return;
		case CourseworkPackage.COURSE__COURSE_CREDIT:
			setCourseCredit(COURSE_CREDIT_EDEFAULT);
			return;
		case CourseworkPackage.COURSE__PERSON_COORDINATOR:
			getPersonCoordinator().clear();
			return;
		case CourseworkPackage.COURSE__PERSON_LECTURE:
			getPersonLecture().clear();
			return;
		case CourseworkPackage.COURSE__EVALUATIONFORM:
			getEvaluationform().clear();
			return;
		case CourseworkPackage.COURSE__COURSEWORK:
			getCoursework().clear();
			return;
		case CourseworkPackage.COURSE__TIMETABLE:
			setTimetable((Timetable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CourseworkPackage.COURSE__COURSE_NAME:
			return COURSE_NAME_EDEFAULT == null ? courseName != null : !COURSE_NAME_EDEFAULT.equals(courseName);
		case CourseworkPackage.COURSE__COURSE_CODE:
			return COURSE_CODE_EDEFAULT == null ? courseCode != null : !COURSE_CODE_EDEFAULT.equals(courseCode);
		case CourseworkPackage.COURSE__COURSE_DESCRIPTION:
			return COURSE_DESCRIPTION_EDEFAULT == null ? courseDescription != null
					: !COURSE_DESCRIPTION_EDEFAULT.equals(courseDescription);
		case CourseworkPackage.COURSE__COURSE_CREDIT:
			return courseCredit != COURSE_CREDIT_EDEFAULT;
		case CourseworkPackage.COURSE__PERSON_COORDINATOR:
			return personCoordinator != null && !personCoordinator.isEmpty();
		case CourseworkPackage.COURSE__PERSON_LECTURE:
			return personLecture != null && !personLecture.isEmpty();
		case CourseworkPackage.COURSE__EVALUATIONFORM:
			return evaluationform != null && !evaluationform.isEmpty();
		case CourseworkPackage.COURSE__COURSEWORK:
			return coursework != null && !coursework.isEmpty();
		case CourseworkPackage.COURSE__TIMETABLE:
			return timetable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (courseName: ");
		result.append(courseName);
		result.append(", courseCode: ");
		result.append(courseCode);
		result.append(", courseDescription: ");
		result.append(courseDescription);
		result.append(", courseCredit: ");
		result.append(courseCredit);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
