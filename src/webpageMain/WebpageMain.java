package webpageMain;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import no.tru.coursework.Course;
import no.tru.coursework.CourseWork;
import no.tru.coursework.CourseworkFactory;
import no.tru.coursework.CourseworkPackage;
import no.tru.coursework.Person;




public class WebpageMain {

	private static void registerEMFStuff() {
		// registers the Transportation model's package object on its unique URI
		EPackage.Registry.INSTANCE.put(CourseworkPackage.eNS_PREFIX, CourseworkPackage.eINSTANCE);
		// register our Resource factory for the xmi extension, so the right Resource implementation is used
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());
	}
	
	public static void main(String[] args) {
		registerEMFStuff();

		// create objects using generated API
		// The factory (singleton) object is used for creating instances  
		CourseworkFactory factory = CourseworkFactory.eINSTANCE;
		Course t = factory.createCourse();
		CourseWork c1 = factory.createCourseWork();
		t.getCoursework().add(c1);
		
		Person p1 = factory.createPerson();
		t.getPersonCoordinator().add(p1);
		

		// create objects by loading file
		ResourceSet resourceSet = new ResourceSetImpl();
		URI uri = URI.createURI(WebpageMain.class.getResource("CourseTDT4100.xmi").toString());
		Resource resource = resourceSet.getResource(uri, true);
		try {
			for (EObject root : resource.getContents()) {
				Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
				if (diagnostics.getSeverity() != Diagnostic.OK) {
					System.err.println(diagnostics.getMessage());
					for (Diagnostic child : diagnostics.getChildren()) {
						System.err.println(child.getMessage());						
					}
				} else {
					System.out.println("No problems with " + root);
				}
			}
		} catch (RuntimeException e) {
			System.out.println(e);
		}
	}

}
