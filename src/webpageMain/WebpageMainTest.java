package webpageMain;

import java.util.List;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import no.tru.coursework.Course;
import no.tru.coursework.CourseworkFactory;
import no.tru.coursework.CourseworkPackage;
import no.tru.coursework.EvaluationForm;



public class WebpageMainTest {

	@Before
	public void registerEMFStuff() {
		// registers the Transportation model's package object on its unique URI
		EPackage.Registry.INSTANCE.put(CourseworkPackage.eNS_PREFIX, CourseworkPackage.eINSTANCE);
		// register our Resource factory for the xmi extension, so the right Resource implementation is used
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());
	}

	private List<EObject> loadResource(String name) {
		ResourceSet resourceSet = new ResourceSetImpl();
		URI uri = URI.createURI(getClass().getResource(name).toString());
		Resource resource = resourceSet.getResource(uri, true);
		EList<EObject> contents = resource.getContents();
		Assert.assertTrue(resource.getErrors().isEmpty());
		return contents;
	}

	@Test
	public void testValidation1() {
		for (EObject root : loadResource("CourseTDT4100.xmi")) {
			Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
			Assert.assertTrue(diagnostics.getSeverity() == Diagnostic.OK);
		}
	}

	@Test
	public void testValidation2() {
		for (EObject root : loadResource("CourseTDT4250.xmi")) {
			Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
			Assert.assertFalse(diagnostics.getSeverity() == Diagnostic.OK);
		}
	}

	@Test
	public void testEvaluationWeight() {
		CourseworkFactory factory = CourseworkFactory.eINSTANCE;
		Course course = factory.createCourse();
		EvaluationForm ev1 = factory.createEvaluationForm();
		ev1.setType("work");
		ev1.setWeight(.4);
		EvaluationForm ev2 = factory.createEvaluationForm();
		ev2.setType("examination");
		ev2.setWeight(.6);
		course.getEvaluationform().add(ev1);
		course.getEvaluationform().add(ev2);
		
		double totalWeight = 0;
		for (int i=0; i<course.getEvaluationform().size(); i++) {
			EvaluationForm evtest = course.getEvaluationform().get(i);
			totalWeight += (double) (evtest.getWeight()) ;
		}
		Assert.assertEquals(1.0, totalWeight, 0.0);
	}
	

}
