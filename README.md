# TDT4250 Course work: Modeling information in course web page
Get to know EMF

## Model
- Course - container for Person, CourseWork, EvaluationForm and TimeTable
- Person - person who is either coordinator, lecturer or both for a course
- CourseWork - workload for the course
- EvaluationForm - how the grade in the course is decided
- TimeTable - time and place for courseWork
- Department - container for Course